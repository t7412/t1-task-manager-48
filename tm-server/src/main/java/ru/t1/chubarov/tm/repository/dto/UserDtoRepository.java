package ru.t1.chubarov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chubarov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("SELECT p FROM UserDTO p ", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(1)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM UserDTO p WHERE p.id = :id", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public void remove(@NotNull UserDTO model) {
        entityManager
                .createQuery("DELETE FROM UserDTO p WHERE p.id = :id")
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM UserDTO p", Integer.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM UserDTO p WHERE p.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }
    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT p.id, p.role, p.email, p.login, p.firstName, p.lastName, p.middleName, p.locked, p.passwordHash FROM UserDTO p WHERE p.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull String login) {
        return entityManager
                .createQuery("SELECT COUNT(p) > 0 FROM UserDTO p WHERE p.login = :login", Boolean.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull String email) {
        return entityManager
                .createQuery("SELECT COUNT(p) > 0 FROM UserDTO p WHERE p.email = :email", Boolean.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getSingleResult();
    }

}
