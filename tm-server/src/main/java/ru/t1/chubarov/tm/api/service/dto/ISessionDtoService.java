package ru.t1.chubarov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionDtoService {
    @NotNull
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception;

    @NotNull
    List<SessionDTO> findAll() throws Exception;

    @Nullable
    List<SessionDTO> findAll(@NotNull String userId) throws Exception;

    @NotNull
    SessionDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    SessionDTO remove(@NotNull String userId, @Nullable SessionDTO model) throws Exception;

    void removeAll(@NotNull String userId, @Nullable Collection<SessionDTO> models) throws Exception;

    @NotNull
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    SessionDTO removeOneById(@Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    int getSize() throws Exception;

}
